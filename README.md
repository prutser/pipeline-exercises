# pipeline-training

Working with git and letting pipelines do the heavy lifting is a common practice nowadays, so here are some exercises to get to know the techniques. In this session we'll get to grips with building, testing and generally doing "stuff" with pipelines attached to git repositories. Most of the info that you'll need can be found in this [Reference](https://docs.gitlab.com/ee/ci/yaml/README.html "GitLab CI/CD Pipeline Configuration Reference"), I advice you to use the right side navigation pane "On this page:" and "Ctrl+f" on that page. Where applicable I'll try linking to the specific parts of that document. If all goes to plan at the end of this you'll have some more experience with a full blown multi stage build, test, deploy sytem with approval gates like the ones used by most big projects. ([Mailman](https://gitlab.com/mailman/mailman/pipelines), [GnuTLS](https://gitlab.com/gnutls/gnutls/pipelines), [Inkscape](https://gitlab.com/inkscape/inkscape/pipelines), [F-Droid](https://gitlab.com/fdroid/fdroidclient/pipelines))

## Contents

1. How does it work
2. Fork this repo
3. Bash commands
4. Artifacts
5. Multi stage
6. Approvals

## How does it work

Basically it all started with [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks), they provide a way to run predefined bits of code at certain git state changes, and can be configured locally to do things like linting before pushing code to the remote or on the remote to build standardized packages from your source code.

### Git hooks

A list of all the git hooks and where they are executed.

#### local

- applypatch-msg
- pre-applypatch
- post-applypatch
- pre-commit
- prepare-commit-msg
- commit-msg
- post-commit
- pre-rebase
- post-checkout
- post-merge
- pre-receive

#### remote

- update **This is where owners prevent you from pushing to master ;-)**
- post-receive **This is where most pipelines hook into**
- post-update
- pre-auto-gc
- post-rewrite
- pre-push

## Fork this repo

Make sure you are logged in to gitlab.com, if you don't have an account create one [here](https://gitlab.com/users/sign_in#register-pane).

Visit this url: [Fork me](https://gitlab.com/cloud-people/pipeline-exercises/-/forks/new) and choose a destination to fork it to, usualy your own gitlab username.
Clone the resulting repo locally and have a look at some basic git hook examples and how these work with:

```bash
ls -lah .git/hooks
```

> For bonus points try to edit some of the scripts and get it to do something useless and fun.

## Bash commands

At this point you should have a fork of the original repo cloned on your machine, let's start with creating a basic pipeline that executes a few bash commands to get a feel for the process.

### `.gitlab-ci.yml`

 > Create this file in the root of the repo

This is a super minimal gitlab pipeline that just prints the date.

```yaml
image: ubuntu

pipeline:
  script:
    - date
```

### what's in this yml

As you can see .gitlab-ci.yml uses the very popular [yaml](https://learnxinyminutes.com/docs/yaml/) [syntax](https://yaml.org/) let's see what is defined in this file:

- **image:** Here you specify which [docker image](https://hub.docker.com/_/ubuntu) to use for your pipeline run.
- **pipeline:** This can be anything as it names your job.
- **script:** This is a mandatory and takes in a `list` of `single line` bash commands.
- **- date** An item in the list of script items, runs the date command.

Add and commit this new file, push it to gitlab.

```bash
git add .gitlab-ci.yml
git commit -m "Added gitlab-ci.yml"
git push
```

Take a look at the CI/CD > Pipelines tab.
To see your job output click on the build number which should also be tagged latest.
Click on the Jobs tab and click on the Job ID.

#### Congrats, you've just created a gitlab pipeline ;-)

> Now let's put that thing to use!

Pipelines can do the stuff you're to lazy to do yourself like:

- compiling source code
- run linting
- run tests
- packaging software ready for consumption
- security testing containers
- deploy the application to a cloud provider or container platform.
- load test your application to see if it is ready for real use

### Compile source code

For this example we'll use some oldschool C source code, add the below code to hello.c in this repo.

```C
#include<stdio.h>            //Pre-processor directive
int main()                   //main function declaration
{
  printf("Hello World!\n");  //to output the string on a display
  return 0;                  //terminating function
}
```

Compile this locally.

```Bash
gcc hello.c -o hello
```

Run the result.

```txt
# ./hello
Hello World!
#
```

Now this is pretty simple, the project you're thinking about applying this to is probably way more complicated but the principal stays the same.
Taking some source and if needed pull in dependencies then make it ready to be run by the end user.
Now let's have a look at letting our fresh new pipeline build this for us by adding some more commands to `.gitlab-ci.yml`

```yaml
image: ubuntu

pipeline:
  script:
    - gcc hello.c -o hello
    - ./hello
```

Commit this updated `.gitlab-ci.yml` and add the source code. Have a look at the output in the gitlab web interface.

```bash
git add .gitlab-ci.yml hello.c
git commit -m "Updated gitlab-ci.yml to run hello.c"
git push
```

---

Well that was embarrassing, or was it?

Working with pipelines you often need to think about how you can get the runner to the point where it can do it's job like compiling some C code.
Adding features to your pipelines usually is a repeat of this process; add code, check result, adjust till it works.
Now try to figure out how fix your pipeline and celebrate your victory!
> Hint: what is missing and how can you install that with `.gitlab-ci.yml`?

Commit your updated `.gitlab-ci.yml` and have a look at the output in the gitlab web interface.

Installing and managing some dependencies for one pipeline isn't a big deal but if you manage 10's, 100's or even more pipelines you might want to centralize some of that heavy lifting and use a different base image that includes some build tools. For example:
- https://gitlab.com/aapjeisbaas/builder
  - IMAGE: aapjeisbaas/builder
- https://gitlab.com/conclusionxforce/container/tools-container
  - IMAGE: registry.gitlab.com/conclusionxforce/container/tools-container:latest
- https://gitlab.com/jeroenvdl/tools-container
  - IMAGE: registry.gitlab.com/jeroenvdl/tools-container:latest

## Artifacts

After building this awesome hello world binary we might want to show this off to the world by saving it as an artifact of our pipeline.
Have a go at exporting your `hello` binary [RTFM](https://docs.gitlab.com/ee/ci/yaml/#artifacts) ;-)

Artifacts can also be very useful to store build log files to be able to better debug failed builds.


## Multi stage pipelines

Building testing and deploying thing can be in one pipeline but it makes it harder to track down were errors are comming up so let's try to do this in sepperate steps.

### Build

This step is already in your ci yml and in this tutorial was named pipeline, let's change it to build and add another step called test.

```yml
image: ubuntu

build:
  script:
    - ...
    - ...
    - gcc....

test:
  script:
    - ./test.sh
```

### Test

Now that we have 2 stages in our pipeline let's test our binary

Test script:

```bash
#!/bin/bash

./hello | grep 'Hello World!' &> /dev/null
if [ $? == 0 ]; then
    echo "Output matches expectations."
    EXIT_CODE=0
else
    echo "Hello is broken"
    EXIT_CODE=1
fi

exit ${EXIT_CODE}
```

### Package it

We'll use docker to package this binary add the bellow Dockerfile.

```txt
FROM ubuntu
COPY hello /
CMD ["/hello"]
```

To be able to use nested docker we need to enable this in our pipeline stage, add this package stage to your pipeline.

```yaml
package:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker build -t ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} .
    - docker push     ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
```

After the pipeline completed with this docker package step you'll find an image in your gitlab registry in the left sidebar under Package >> Container Registry.

Try to run your container by copying the image uri and run it as shown below:

```txt
# docker run -ti registry.gitlab.com/aapjeisbaas/pipeline-exercises/master:85b3a493619a30aa1be3b3518eeacd01a0136884
Unable to find image 'registry.gitlab.com/aapjeisbaas/pipeline-exercises/master:85b3a493619a30aa1be3b3518eeacd01a0136884' locally
85b3a493619a30aa1be3b3518eeacd01a0136884: Pulling from aapjeisbaas/pipeline-exercises/master
c9b1b535fdd9: Already exists 
47f6c4cf637e: Pull complete 
Digest: sha256:8fb17c32ce252b2f179074dca4edeeca5835e400ebcc89e66a565a513976e935
Status: Downloaded newer image for registry.gitlab.com/aapjeisbaas/pipeline-exercises/master:85b3a493619a30aa1be3b3518eeacd01a0136884
Hello World!

```

## Approvals

We can build test and package, now for a final manual aproval step to release the docker image.

```yml
push_container:
  stage: deploy
  image: docker:latest
  services:
  - docker:dind
  rules:
    - when: manual
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG
    CI_APPLICATION_TAG: $CI_COMMIT_SHA
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker pull ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}
    - docker tag ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
```

Trigger the manual step after you've checked your image actualy works.

As a last step you'll be able to run:
`docker run registry.gitlab.com/$GITLAB_USERNAME/pipeline-exercises:latest`

---

If you have some time, consider building a tools / build container for yourself.

#### Examples
- https://gitlab.com/aapjeisbaas/builder/
- https://gitlab.com/conclusionxforce/container/tools-container